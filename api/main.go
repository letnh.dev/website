package main

import (
	"fmt"
	"log"
	"net/http"
)

const port = ":8080"

func main() {
  mux := http.NewServeMux()


  mux.HandleFunc("/", baseHandler)

  srv := http.Server{
    Addr: port,
    Handler: mux,
  }

  log.Printf("Sever started at port: %v", port)
  log.Fatal(srv.ListenAndServe())
}

func baseHandler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprint(w, "Under development!")
}
