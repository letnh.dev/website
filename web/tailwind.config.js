module.exports = {
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        letnh: {
          text: '#B3ADAD',
          bg: '#383C3E',
        }
      }
    },
  },
  corePlugins: {
    preflight: false,
  },
  plugins: [],
}
